# Lambe Instragram Clone

Welklin Ribeiro

#Using GITFLOW

`https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html`

Já estou usando o GitFlow

### Dependencies and React Native Package Manager

to package.json:

```css
  "dependencies": {
      "react": "16.4.1",
      "react-native": "0.56.0",
      "axios": "0.18.0",
      "react-native-gravatar": "1.0.2",
      "react-native-image-picker": "0.27.1",
      "react-native-vector-icons": "5.0.0",
      "react-navigation": "2.14.2",
      "react-redux": "5.0.7",
      "redux": "4.0.0",
      "redux-thunk": "2.3.0"
    },
    .... end of file:
    ,
    "rnpm":{
      "assets": [
        "./assets/fonts/"
      ]
    }
```

### Assets

http://files.cod3r.com.br/curso-react-native/assets_lambe.zip

### Problem with javac

```
update-alternatives --list java
sudo xed ~/.bashrc
```

put in EOF

```
#export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

check javac -version  
if need:

```
sudo apt install openjdk-8-jdk-headless
```

## react-native-image-picker

### Android: AndroidManifest:

```
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```

### build:gradle

```
comentar o classpath 'com.android.tools.build:gradle:2.3.3'
Adicionar o classpath 'com.android.tools.build:gradle:2.2.+'
```

### IOS: info.plist

Adicionar:

```
	<key>NSPhotoLibraryUsageDescription</key>
	<string>$(PRODUCT_NAME) would like access to your photo gallery</string>
	<key>NSCameraUsageDescription</key>
	<string>$(PRODUCT_NAME) would like to use your camera</string>
	<key>NSPhotoLibraryAddUsageDescription</key>
	<string>$(PRODUCT_NAME) would like to save photos to your photo gallery</string>
```

## Firebase RealTime DataBase

### https://lambe-bff75.firebaseio.com/TODO usar

## Firebase Functions
TODO usar
TODO usarprojeto:
TODO usar
TODO usar
TODO usarse-tools

$ firebase login    (se der erro: $ alias firebase="`npm config get prefix`/bin/firebase")

$ firebase init
  -functions (selecionar com o teclado)
  -selecionar o projeto
  -Javascript
  -Yes (eslint)
  -Yes (Dependencies)

entrar na pasta functions:

`/functions$ npm i -s @google-cloud/storage cors uuid-v4`


está pronto para desenvolver as funções em functions.

### Gerar arquivo json

console.firebase -> projeto -> configurações -> contas de serviço-> Node.js -> gerar nova chave privada
Salvar o arquivo em functions

### Nome do Bucket

    no firebase console ir em:
      - storage
      - getstarted 
      - ok
      - *lambe-bff75.appspot.com* este é o nome do bucket

### Deploy

    sair da pasta functions:
    ~~~~
    $firebase deploy --project lambe-bff75 (nome do projeto)
    ~~~~

### URL Base

    https://us-central1-lambe-bff75.cloudfunctions.net/uploadImage

### Protegendo Rotas Firebase

{
  "rules": {
    ".read": true,
    ".write": "auth != null",
  	"users": {
      "$id":{
        ".write":"!data.exists()"
      }
    }
  }
}
