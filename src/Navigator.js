import React from 'react';
import { Text, View } from 'react-native';
import {
    createBottomTabNavigator,
    createAppContainer,
    createSwitchNavigator,
    createStackNavigator,
} from 'react-navigation';

import Splash from './screens/Splash'
import Feed from './screens/Feed'
import AddPhoto from './screens/AddPhoto'
import Profile from './screens/Profile'
import Login from './screens/Login'
import Register from './screens/Register'

import Icon from 'react-native-vector-icons/FontAwesome'

const authRouter = createStackNavigator({
    Login: { screen: Login, navigationOptions: { title: 'Login' } },
    Register: { screen: Register, navigationOptions: { title: 'Register' } }
}, {
        initialRouteName: 'Login'
    })

const loginOrProfileRouter = createSwitchNavigator({
    Auth: authRouter,
    Profile: Profile
}, {
        inicialRouteName: 'Auth'
    })

const MenuNavigator = createBottomTabNavigator({
    Feed: {
        screen: Feed,
        navigationOptions: {
            title: 'Feed',
            tabBarIcon: ({ tintColor }) =>
                <Icon name='home' size={30} color={tintColor} />
        }

    },
    Add: {
        screen: AddPhoto,
        navigationOptions: {
            title: 'Add Photo',
            tabBarIcon: ({ tintColor }) =>
                <Icon name='camera' size={30} color={tintColor} />
        }
    },
    Profile: {
        screen: loginOrProfileRouter,
        navigationOptions: {
            title: 'Profile',
            tabBarIcon: ({ tintColor }) =>
                <Icon name='user' size={30} color={tintColor} />
        }
    },
},
    {
        tabBarOptions: {
            showLabel: false,
        }
    });

// export default TabNavigator;

const SplashRouter = createSwitchNavigator({
    Splash: Splash,
    App: MenuNavigator,
},{
    initialRouteName: 'Splash'    
})

export default SplashRouter